#!/bin/bash
   export APP_HOME=$PWD
   lb web-server list -s
   lb web-server unload-services
   lb web-server load-services
   lb web-server list -s
   lb web-client import http://localhost:8080/dims_tdx/product -i $APP_HOME/data/product_hierarchy.csv
   lb web-client import http://localhost:8080/dims_tdx/location -i $APP_HOME/data/location_hierarchy.csv
   lb web-client import http://localhost:8080/dims_tdx/calendar -i $APP_HOME/data/calendar_hierarchy.csv
   lb web-client import http://localhost:8080/metrics_tdx/forecast -i $APP_HOME/data/forecast.csv
   lb web-client import http://localhost:8080/metrics_tdx/sales -i $APP_HOME/data/sales.csv
